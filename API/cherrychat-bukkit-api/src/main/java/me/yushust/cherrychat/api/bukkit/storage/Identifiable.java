package me.yushust.cherrychat.api.bukkit.storage;

import java.util.UUID;

public interface Identifiable {

    UUID getId();

}
